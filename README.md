# Fake Backend

This is a small application for use in development. It is intended to be used
as a replacement to a proper application backend during development.

## Usage

Build the container:

```shell
podman build -t fakebackend:latest .
```

Once built, the container image can be started with:

```shell
podman run -p 8000:8000 fakebackend:latest
```
