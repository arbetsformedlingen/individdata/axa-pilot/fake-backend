# Install dependencies
FROM node:20.18.1-alpine3.20 as build

WORKDIR /app

COPY package.json package-lock.json ./
RUN npm upgrade -g npm &&\ 
    npm clean-install

COPY . .
RUN npm run build

# Production image
FROM node:20.18.1-alpine3.20

RUN apk add --no-cache tini

WORKDIR /app

COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/dist .

EXPOSE 8000

ENV PORT 8000

ENTRYPOINT [ "/sbin/tini", "--" ]
CMD [ "node", "index.js" ]
