import express, { Application } from "express";
import pino from "pino-http";
import { randomUUID } from "crypto";

const app: Application = express();
const port = process.env.PORT || 8000;

app.use(pino());
app.use(express.urlencoded({ extended: false }));

app.get("/service/:clientId/", async (req, res) => {
  res.json({ clientId: req.params.clientId, name: 'Företag AB', redirectUrl: 'https://example.com/redirect' })
})

app.get("/user/giveConsent2", async (req, res) => {
  res.json({ clientId: req.query.clientId, name: 'Arthur Dent', enabled: true, authCode: randomUUID(), redirectUrl: 'https://example.com/redirect' })
});

app.get("/user/revokeConsent2", async (req, res) => {
  res.json({ clientId: req.query.clientId, name: '', enabled: false, authCode: '', redirectUrl: 'https://example.com/redirect' })
});

app.listen(port, () => {
  console.log(`Server is running at port ${port}.`);
});
